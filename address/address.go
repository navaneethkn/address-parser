package address

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Address represents a parsed address data
type Address struct {
	Street  string `json:"street"`
	HouseNo string `json:"housenumber"`
}

func (a *Address) String() string {
	b, err := json.Marshal(a)
	if err != nil {
		return fmt.Sprintf("Failed to marshal JSON. %v", err)
	}

	return string(b)
}

// Parser does parsing
type Parser struct {
	Tokenizer Tokenizer

	// parser may need to correct the previous token type and add to the correct group
	prev *token

	// buffer to keep accepted tokens
	selected []*token
}

// Parse parses text and return well formated addresses
func (p *Parser) Parse() (*Address, error) {
	p.Tokenizer.Tokenize()

	token := p.Tokenizer.Next()
	for ; token != nil; token = p.Tokenizer.Next() {
		switch token.typ {
		case tokenAlphabet:
			token.ptyp = tokenStreet
			if p.prev != nil && p.prev.ptyp == tokenHouseNo && p.Tokenizer.LastToken() {
				// if the previous token is part of house number and this is the last token, this will be part of house number
				token.ptyp = tokenHouseNo
			}
		case tokenNumeric, tokenAlphaNumeric, tokenKeywordHouseNumber:
			token.ptyp = tokenHouseNo
			if p.prev != nil && p.prev.ptyp == tokenStreet && p.Tokenizer.PendingTokens() > 1 {
				// assuming a house number can have only 2 parts
				token.ptyp = tokenStreet
			}

		}
		// a house number keyword such as "No". This will be most likely part of house number
		// if this keyword appeared in between a street name eg: "No one's street, 3054", it should be part of street name

		p.selected = append(p.selected, token)
		p.prev = token
	}

	return &Address{Street: flattenTokens(p.selected, tokenStreet), HouseNo: flattenTokens(p.selected, tokenHouseNo)}, nil
}

func flattenTokens(tokens []*token, ptyp parsedTokenType) string {
	str := ""
	for _, token := range tokens {
		if token.ptyp == ptyp {
			str = fmt.Sprintf("%s %s", str, token.text)
		}
	}
	return strings.TrimSpace(str)
}
