package address

import (
	"regexp"
	"strings"
)

// A SpaceTokenizer tokenizes the input using SPACE as the separator
type SpaceTokenizer struct {
	// Represents the input text which will be tokenized
	Input string

	tokenized bool
	tokens    []*token
	pos       int
}

var (
	validNumber       = regexp.MustCompile(`^[0-9]*$`)
	validAlphabet     = regexp.MustCompile(`^[a-zA-Z]*$`)
	validAlphaNumeric = regexp.MustCompile(`^[a-zA-Z0-9]*$`)
)

func (t *SpaceTokenizer) Tokenize() {
	parts := strings.Split(t.Input, " ")
	for _, p := range parts {
		p = sanitize(p)
		typ := tokenAlphabet
		if isKeyword(p) {
			typ = tokenKeywordHouseNumber
		} else if validNumber.MatchString(p) {
			typ = tokenNumeric
		} else if validAlphabet.MatchString(p) {
			typ = tokenAlphabet
		} else if validAlphaNumeric.MatchString(p) {
			typ = tokenAlphaNumeric
		}
		t.tokens = append(t.tokens, &token{typ: typ, text: p})
	}
}

func (t *SpaceTokenizer) Next() *token {
	if t.pos == len(t.tokens) {
		return nil
	}

	token := t.tokens[t.pos]
	t.pos++
	return token
}

func (t *SpaceTokenizer) HasMore() bool {
	return t.pos+1 < len(t.tokens)
}

func (t *SpaceTokenizer) PendingTokens() int {
	return len(t.tokens) - t.pos
}

func (t *SpaceTokenizer) LastToken() bool {
	return t.pos == len(t.tokens)
}

func sanitize(text string) string {
	return strings.Replace(text, ",", "", -1)
}
