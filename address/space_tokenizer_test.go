package address

import "testing"

func TestTokenize(t *testing.T) {
	tokenizer := &SpaceTokenizer{
		Input: "200 broadway av",
	}

	tokenizer.Tokenize()

	if len(tokenizer.tokens) != 3 {
		t.Errorf("Expected 3 tokens, got %d", len(tokenizer.tokens))
	}

	tok1 := tokenizer.tokens[0]
	if tok1.typ != tokenNumeric {
		t.Errorf("Expected tokenNumeric, got %d", tok1.typ)
	}

	tok2 := tokenizer.tokens[1]
	if tok2.typ != tokenAlphabet {
		t.Errorf("Expected tokenAlphabet, got %d", tok2.typ)
	}

	tok3 := tokenizer.tokens[2]
	if tok3.typ != tokenAlphabet {
		t.Errorf("Expected tokenAlphabet, got %d", tok3.typ)
	}
}
