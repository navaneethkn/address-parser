package address

import "strings"

// lexical token type
type tokenType int

// actual token type determined by the parser
type parsedTokenType int

const (
	tokenAlphabet tokenType = iota
	tokenAlphaNumeric
	tokenNumeric
	tokenKeywordHouseNumber
)

const (
	tokenUnknown parsedTokenType = iota
	tokenStreet
	tokenHouseNo
)

var (
	// all keywords which can come with a house number. this is case insensitive
	houseNumberKeywords = []string{"no", "#"}
)

// token represents a single unit in the input text
type token struct {
	// tokenizer sets this token type
	typ tokenType

	// parser sets ptype after determining if the token is a street or hno
	ptyp parsedTokenType

	text string
}

// Tokenizer is the interface that wraps common tokenization functions
type Tokenizer interface {
	Tokenize()
	Next() *token
	HasMore() bool
	PendingTokens() int
	LastToken() bool
}

func isKeyword(text string) bool {
	for _, keyword := range houseNumberKeywords {
		if ok := strings.EqualFold(keyword, text); ok {
			return true
		}
	}
	return false
}
