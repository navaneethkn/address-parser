package address

import (
	"testing"
)

func TestParsing(t *testing.T) {
	var tests = []struct {
		input    string
		expected *Address
	}{
		{"200 Broadway Av", &Address{Street: "Broadway Av", HouseNo: "200"}},
		{"Winterallee 3", &Address{Street: "Winterallee", HouseNo: "3"}},
		{"Blaufeldweg 123B", &Address{Street: "Blaufeldweg", HouseNo: "123B"}},
		{"Am Bächle 23", &Address{Street: "Am Bächle", HouseNo: "23"}},
		{"Auf der Vogelwiese 23 b", &Address{Street: "Auf der Vogelwiese", HouseNo: "23 b"}},
		{"4, rue de la revolution", &Address{Street: "rue de la revolution", HouseNo: "4"}},
		{"Calle Aduana, 29", &Address{Street: "Calle Aduana", HouseNo: "29"}},
		{"Calle 39 No 1540", &Address{Street: "Calle 39", HouseNo: "No 1540"}},
		{"No 1540 calle st", &Address{Street: "calle st", HouseNo: "No 1540"}},
		{"9 Saint William St", &Address{Street: "Saint William St", HouseNo: "9"}},
	}
	for _, tt := range tests {
		t.Run(tt.input, func(t *testing.T) {
			parser := &Parser{
				Tokenizer: &SpaceTokenizer{
					Input: tt.input,
				},
			}
			addr, _ := parser.Parse()
			if *addr != *tt.expected {
				t.Errorf("expecting %s, got %s", tt.expected, addr)
			}

		})
	}
}
