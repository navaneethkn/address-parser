package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/navaneethkn/address-parser/address"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		log.Fatalf("usage: address-parser inputfile.txt")
	}

	contents, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("error reading %s. %v", args[0], err)
	}

	lines := strings.Split(string(contents), "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		parser := &address.Parser{
			Tokenizer: &address.SpaceTokenizer{
				Input: line,
			},
		}
		addr, err := parser.Parse()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%s = %s\n", line, addr)
	}
}
