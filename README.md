# Address parser

## Getting started

```
$ go get gitlab.com/navaneethkn/address-parser
$ address-parser inputfile.txt
```

`inputfile.txt` can have all the address text which needs to be parsed.

Running tests

```
go test -v ./address
```

Will run all the tests contained in the address package.

## Design

Parsing addresses is a complex problem. Addresses can be of all forms and each country follows their own pattern for defining address. So clearly, designing a generic address parser is very ambitious and may not be realistic.

This design gives priority to the examples shown in the problem description. It may work for similar patterns.

Address parser implementation is not using Regular expressions as a primary means for parsing. Instead, this package implements a simple state machine which can be used to make sense of the address text. Using a simple state machine has many advantages. 

1. Implementation becomes very easy to understand and extend
2. Regular expressions tends to become complex and hard to debug at times. A state machine based approach avoids this pitfall
3. Contextual rules can be added in state machine implementation as each token is parsed from left to right. Backtracking and providing meaningful conversions are also possible with the state machine based approach. 

## Code structure

Address parser is implemented using `Go` programming language.

Implementation mainly consists of two packages.

1. `address` package provides a tokenizer interface, a default tokenizer which tokenizes using space character and a left to right parser which segregates tokens into street and house number attributes
2. `main` package which uses the `address` package
